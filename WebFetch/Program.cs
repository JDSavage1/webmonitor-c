﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using System.Net.Mail;


/// <summary>
/// Fetches a Web Page
/// </summary>
class WebFetch
{
    public SmtpClient client = new SmtpClient();
    public MailMessage msg = new MailMessage();
    public System.Net.NetworkCredential smtpCreds = new System.Net.NetworkCredential("email", "password");
   /*
    TEST TEST TEST
    */
    
    public bool status { get; set; }
    int statusCode;
    int num = 1;
    string contentType, responseUri, statusDescription, message;
    public static DateTime Now { get; set; }

    static void Main(string[] args)
    {
        WebFetch stat = new WebFetch();
        stat.GetWebsiteInfo();
        if (stat.status)
        {
            WebFetch send = new WebFetch();
            Console.WriteLine(stat.toString());
        }
        else
        {
            WebFetch send = new WebFetch();
            Console.WriteLine(stat.toString());
            send.SendEmail( "email1", "email2", "from email", "title", "message");
           
        }

    }


    public string toString()
    {
        Now = DateTime.Now;
        return "Message: " + message + "\n" +
           "Date: " + Now + "\n" +
           "Status Code: " + statusCode + "\n" +
           "Status Description: " + statusDescription + "\n" +
           "Response Uri: " + responseUri + "\n" +
           "Content Type " + contentType + "\n";
    }

    public void SendEmail(string sendTo, string sendTo2, string sendFrom, string subject, string body)
    {
        try
        {
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.Credentials = smtpCreds;
            client.EnableSsl = true;

            MailAddress to = new MailAddress(sendTo);
            msg.To.Add(to);
            to = new MailAddress(sendTo2);
            msg.To.Add(to);

            MailAddress from = new MailAddress(sendFrom);
            msg.Subject = subject;
            msg.Body = body;
            msg.From = from;
            msg.To.Add(to);

            client.Send(msg);

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message, "Error");
        }
    }

    public void GetWebsiteInfo()
    {
        // used to build entire input
        StringBuilder sb = new StringBuilder();

        // used on each read operation
        byte[] buf = new byte[8192];

        // prepare the web page we will be asking for
        


        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://acaexpress.com");
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // we will read data via the response stream

            Stream resStream = response.GetResponseStream();

            string tempString = null;
            int count = 0;

            do
            {
                // fill the buffer with data
                count = resStream.Read(buf, 0, buf.Length);

                // make sure we read some data
                if (count != 0)
                {
                    // translate from bytes to ASCII text
                    tempString = Encoding.ASCII.GetString(buf, 0, count);

                    // continue building the string
                    sb.Append(tempString);
                }
            }
            // any more data to read?
            while (count > 0);
            string text = sb.ToString();


            if (text.Contains("ACAExpress and is not the Health Insurance Marketplace website."))
            {

                //Console.WriteLine(num+ ": Website is Up");
                this.status = true;
                message = "ACAExpress is online.";
                contentType = response.Method;
                responseUri = response.ResponseUri.ToString();
                statusCode = (int)response.StatusCode;
                statusDescription = response.StatusDescription;
                num++;
            }
            else
            {
                //Console.WriteLine("Website down");
                message = "1.ACAExpress is offline.";
                contentType = response.Method;
                responseUri = response.ResponseUri.ToString();
                statusCode = (int)response.StatusCode;
                statusDescription = response.StatusDescription;
                this.status = false;

            }
            
        }
        catch (WebException ex)
        {
            if (ex.Status == WebExceptionStatus.ProtocolError &&
                ex.Response != null)
            {
                var response = (HttpWebResponse)ex.Response;
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    message = "2.Recieved " + response.StatusCode + " error";
                    contentType = response.Method;
                    responseUri = response.ResponseUri.ToString();
                    statusCode = (int)response.StatusCode;
                    statusDescription = response.StatusDescription;
                }
                else
                {
                    message = "3.Recieved " + response.StatusCode + " error";
                    contentType = response.Method;
                    responseUri = response.ResponseUri.ToString();
                    statusCode = (int)response.StatusCode;
                    statusDescription = response.StatusDescription;
                }
            }
            else
            {
                message = "TimedOut";
                Console.WriteLine("4");
            }
        }
    }
}



